# multiAgents.py
# --------------
# Licensing Information: Please do not distribute or publish solutions to this
# project. You are free to use and extend these projects for educational
# purposes. The Pacman AI projects were developed at UC Berkeley, primarily by
# John DeNero (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# For more info, see http://inst.eecs.berkeley.edu/~cs188/sp09/pacman.html

from util import manhattanDistance
from game import Directions
import random, util

from game import Agent

class ReflexAgent(Agent):
  """
    A reflex agent chooses an action at each choice point by examining
    its alternatives via a state evaluation function.

    The code below is provided as a guide.  You are welcome to change
    it in any way you see fit, so long as you don't touch our method
    headers.
  """


  def getAction(self, gameState):
    """
    You do not need to change this method, but you're welcome to.

    getAction chooses among the best options according to the evaluation function.

    Just like in the previous project, getAction takes a GameState and returns
    some Directions.X for some X in the set {North, South, West, East, Stop}
    """
    # Collect legal moves and successor states
    legalMoves = gameState.getLegalActions()

    # Choose one of the best actions
    scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
    bestScore = max(scores)
    bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
    chosenIndex = random.choice(bestIndices) # Pick randomly among the best

    "Add more of your code here if you want to"

    return legalMoves[chosenIndex]

  def evaluationFunction(self, currentGameState, action):
    """
    Design a better evaluation function here.

    The evaluation function takes in the current and proposed successor
    GameStates (pacman.py) and returns a number, where higher numbers are better.

    The code below extracts some useful information from the state, like the
    remaining food (oldFood) and Pacman position after moving (newPos).
    newScaredTimes holds the number of moves that each ghost will remain
    scared because of Pacman having eaten a power pellet.

    Print out these variables to see what you're getting, then combine them
    to create a masterful evaluation function.
    """
    # Useful information you can extract from a GameState (pacman.py)
    successorGameState = currentGameState.generatePacmanSuccessor(action)
    newPos = successorGameState.getPacmanPosition()
    oldFood = currentGameState.getFood()
    newGhostStates = successorGameState.getGhostStates()
    newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]

    "*** YOUR CODE HERE ***"
    lista_comida = successorGameState.getFood().asList()

    distancia_minima_comida = 99999
    for comida in lista_comida:
        distancia_comida = util.manhattanDistance(newPos, comida) 
        if distancia_comida < distancia_minima_comida and distancia_comida != 0:
            distancia_minima_comida = distancia_comida

    distancia_minima_fantasma = 99999
    for estado_fantasma in newGhostStates:
        distancia_fantasma = util.manhattanDistance(newPos, estado_fantasma.getPosition())
        if distancia_fantasma < distancia_minima_fantasma:
            distancia_minima_fantasma = distancia_fantasma
            
    if distancia_minima_fantasma == 0 or distancia_minima_fantasma > 20:
        distancia_minima_fantasma = -1000
        
    return distancia_minima_fantasma/distancia_minima_comida + successorGameState.getScore()

def scoreEvaluationFunction(currentGameState):
  """
    This default evaluation function just returns the score of the state.
    The score is the same one displayed in the Pacman GUI.

    This evaluation function is meant for use with adversarial search agents
    (not reflex agents).
  """
  return currentGameState.getScore()

class MultiAgentSearchAgent(Agent):
  """
    This class provides some common elements to all of your
    multi-agent searchers.  Any methods defined here will be available
    to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

    You *do not* need to make any changes here, but you can if you want to
    add functionality to all your adversarial search agents.  Please do not
    remove anything, however.

    Note: this is an abstract class: one that should not be instantiated.  It's
    only partially specified, and designed to be extended.  Agent (game.py)
    is another abstract class.
  """

  def __init__(self, evalFn = 'scoreEvaluationFunction', depth = '2'):
    self.index = 0 # Pacman is always agent index 0
    self.evaluationFunction = util.lookup(evalFn, globals())
    self.depth = int(depth)

class MinimaxAgent(MultiAgentSearchAgent):
  """
    Your minimax agent (question 2)
  """

  def getAction(self, gameState):
    """
      Returns the minimax action from the current gameState using self.depth
      and self.evaluationFunction.

      Here are some method calls that might be useful when implementing minimax.

      gameState.getLegalActions(agentIndex):
        Returns a list of legal actions for an agent
        agentIndex=0 means Pacman, ghosts are >= 1

      Directions.STOP:
        The stop direction, which is always legal

      gameState.generateSuccessor(agentIndex, action):
        Returns the successor game state after an agent takes an action

      gameState.getNumAgents():
        Returns the total number of agents in the game
    """
    "*** YOUR CODE HERE ***"
    return self.miniMaxStarter(gameState)

  def miniMaxStarter(self, gameState):
      bestAction = ""
      v = -100000
      for action in gameState.getLegalActions():
          prev = v
          v = max(v, self.minValue(gameState.generateSuccessor(0, action), 0, 1))
          if v > prev:
              bestAction = action

      return bestAction

  #Max choice
  def maxValue(self, gameState, depth):
        
    v = -100000#sys.minint

    if gameState.isWin() or gameState.isLose() or depth == self.depth - 1: #bottom has been reached, evaluate
      return self.evaluationFunction(gameState)
    
    #Returns value for each action  
    for action in gameState.getLegalActions(0):
      if action != Directions.STOP:
        v = max(v, self.minValue(gameState.generateSuccessor(0, action), depth, 1))

    return v

  #Min Choice  
  def minValue(self, gameState, depth, numGhost):

    if gameState.isWin() or gameState.isLose() or depth == self.depth - 1: #bottom has been reached, evaluate
      return self.evaluationFunction(gameState)
    
    v = 100000#sys.maxint

    #Run through all ghosts and get their mins, once last ghost is reached and depth hasn't been achieved, max is called  
    for action in gameState.getLegalActions(numGhost):
      if action != Directions.STOP:
        if numGhost == gameState.getNumAgents() - 1:
          v = min(v, self.maxValue(gameState.generateSuccessor(numGhost, action), depth + 1))
        else:
          v = min(v, self.minValue(gameState.generateSuccessor(numGhost, action), depth, numGhost + 1))
    return v

class AlphaBetaAgent(MultiAgentSearchAgent):
  """
    Your minimax agent with alpha-beta pruning (question 3)
  """

  def getAction(self, gameState):
    """
      Returns the minimax action using self.depth and self.evaluationFunction
    """
    "*** YOUR CODE HERE ***"
    return self.alphaBetaStarter(gameState)

  #Starter Function for root node to convert score into action  
  def alphaBetaStarter(self, gameState):  
    bestAction = ""
    v = alpha = -100000
    beta = 10000
    for action in gameState.getLegalActions():
      prev = v
      v = max(v, self.minValue(gameState.generateSuccessor(0, action), alpha, beta, 0, 1)) #calls root children
      if v > prev:
        bestAction = action
      if v >= beta:
        return bestAction
      alpha = max(alpha, v)
        
    return bestAction

  def maxValue(self, gameState, alpha, beta, depth):

    v = -100000

    if gameState.isWin() or gameState.isLose() or depth == self.depth - 1: #bottom has been reached, evaluate
      return self.evaluationFunction(gameState)

    for action in gameState.getLegalActions():
      if action != Directions.STOP:
        v = max(v, self.minValue(gameState.generateSuccessor(0, action), alpha, beta, depth, 1))
        
        if v >= beta:
          return v
          
        alpha = max(alpha, v)

    return v

  def minValue(self, gameState, alpha, beta, depth, numGhost):
    if gameState.isWin() or gameState.isLose() or depth == self.depth - 1:
      return self.evaluationFunction(gameState)

    v = 100000

    for action in gameState.getLegalActions(numGhost):
      if action != Directions.STOP:
        if numGhost != gameState.getNumAgents()-1:
          v = min(v, self.maxValue(gameState.generateSuccessor(numGhost, action), alpha, beta, depth + 1))

        else:
          v = min(v, self.minValue(gameState.generateSuccessor(numGhost, action), alpha, beta, depth, numGhost + 1))

        if v <= alpha:
          return v
        beta = min(beta, v)
    return v

class ExpectimaxAgent(MultiAgentSearchAgent):
  """
    Your expectimax agent (question 4)
  """

  def getAction(self, gameState):
    """
      Returns the expectimax action using self.depth and self.evaluationFunction

      All ghosts should be modeled as choosing uniformly at random from their
      legal moves.
    """
    "*** YOUR CODE HERE ***"
    def minMax(state, checkD = -1, agentsTurn = -1):
        agentsTurn += 1
        agentsTurn = agentsTurn % gameState.getNumAgents()
        if agentsTurn == 0 :
            checkD += 1
        if state.isWin() or state.isLose() or (checkD == self.depth) :
            return self.evaluationFunction(state)
        if agentsTurn == 0 :
            v = maxValue(state, checkD, agentsTurn)
            return v
        else :
            v = minValue(state, checkD, agentsTurn)
            return v
        
    def maxValue(state, checkD, agentsTurn):
        max = float("-inf")
        actions = state.getLegalActions(agentsTurn)
        if checkD == 0:
            max = (max, 'Stop')
            for a in actions :
                successor = state.generateSuccessor(agentsTurn,a)
                v = ( minMax(successor, checkD, agentsTurn), a)
                if v[0] > max[0] :
                    max = v
        else : 
            for a in actions :
                successor = state.generateSuccessor(agentsTurn,a)
                v = minMax(successor, checkD, agentsTurn)
                if v > max :
                    max = v
        return max
    
    def minValue(state, checkD, agentsTurn):
        min = float(0)
        actions = state.getLegalActions(agentsTurn) 
        for a in actions :
            successor = state.generateSuccessor(agentsTurn,a)
            v = minMax(successor, checkD, agentsTurn)
            min += v/len(actions)
        return min

    action = minMax(gameState)
    return action[1]

def betterEvaluationFunction(currentGameState):
  """
    Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
    evaluation function (question 5).

    DESCRIPTION: <write something here so we know what you did>
  """
  newPos = currentGameState.getPacmanPosition()
  newFood = currentGameState.getFood()
  newGhostStates = currentGameState.getGhostStates()
  newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]
  "*** YOUR CODE HERE ***"
  
  def distCloseFood(position, foodGrid) :
    gridInfo = foodGrid.packBits()
    
    value = None
    for i in range(gridInfo[0]) :
      for j in range(gridInfo[1]) :
        if foodGrid[i][j] == True :
          dist = searchAgents.mazeDistance(position, (i,j), currentGameState)
          #dist = ( ( abs(position[0] - i) + abs(position[1] - j) ), (i,j) )
          if value == None :
            value = dist
          if dist < value :
            value = dist
    if value == None : 
      value = (0, position)
    return value
  # returns the distance to the closest food (manhatten distance)
  def distClose(position, foodGrid) :
    gridInfo = foodGrid.packBits()
    
    value = None
    for i in range(gridInfo[0]) :
      for j in range(gridInfo[1]) :
        if foodGrid[i][j] == True :
          dist = ( ( abs(position[0] - i) + abs(position[1] - j) ), (i,j) )
          if value == None :
            value = dist
          if dist[0] < value[0] :
            value = dist
    if value == None : 
      value = (0, position)
    return value

  score = 0
  
  # just addes the game score
  score= currentGameState.getScore()
  
  if currentGameState.isWin() :
    return 1000 + score 
  
  # addes  1/food-count multiplied by 100 to the score if there is food left, if not returns 1000 for a win
  if newFood.count() > 0 :
    score += ( (1/newFood.count() )* 200 )
  
  # if the ghost isn't scared run away, if it is go get em
  for i in range(len(newGhostStates)) :
    ghostPos = newGhostStates[i].getPosition()
    if newScaredTimes[i] < 1 :
      if newPos == (ghostPos[0]-1, ghostPos[1]):
        score = -10000
      elif newPos == (ghostPos[0]+1, ghostPos[1]):
        score = -10000
      elif newPos == (ghostPos[0], ghostPos[1]-1):
        score = -10000
      elif newPos == (ghostPos[0], ghostPos[1]+1):
        score = -10000
    else :
      score += ( (1/( abs(newPos[0] - ghostPos[0]) + abs(newPos[1] - ghostPos[1])) ) * 100 )



  #subtract the distance to the closest food
  score -= distClose(newPos,newFood)[0]
      
  # adds for scared times, making him get capsules
#  for i in range(len(newGhostStates)) :
#      score += newScaredTimes[i] * 10
  

  return score

# Abbreviation
better = betterEvaluationFunction

class ContestAgent(MultiAgentSearchAgent):
  """
    Your agent for the mini-contest
  """

  def getAction(self, gameState):
    """
      Returns an action.  You can use any method you want and search to any depth you want.
      Just remember that the mini-contest is timed, so you have to trade off speed and computation.

      Ghosts don't behave randomly anymore, but they aren't perfect either -- they'll usually
      just make a beeline straight towards Pacman (or away from him if they're scared!)
    """
    "*** YOUR CODE HERE ***"
    util.raiseNotDefined()

